const Courses = require('../model/Course')
const {multipleMongooseToObj} = require('../../util/mongoose');

class UserController {

    userDashboard(req, res, next) {
        res.send('Hê Nhô Bé Vanh');
    }

    userCourses(req, res, next) {
        Courses.find({})
            .then(courses => res.render('user/user-courses', {
                courses: multipleMongooseToObj(courses)
            }))
            .catch(next);
    }
}

module.exports = new UserController();