const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const Schema = mongoose.Schema;
mongoose.plugin(slug);


const Course = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    img: { type: String },
    videoId: { type: String, required: true },
    level: { type: String },
    slug: { type: String, slug: 'name', unique: true },
    time: { type: Date, default: (new Date()).getTime(), required: true },
})
module.exports = mongoose.model('Courses', Course);