const mongoose = require('mongoose');
async function connect() {
  try {
    await mongoose.connect('mongodb://localhost/nodejs_education', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    console.log('Connect successfully');
  } catch (err) {
    console.log('Connect failed');
  }
}

module.exports = { connect };
